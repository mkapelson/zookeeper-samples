package com.nearinfinity.examples.zookeeper.lock;

import com.nearinfinity.examples.zookeeper.util.RandomAmountOfWork;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.ZooKeeper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class DistributedOperationCuratorExecutorTest {

    private static final Logger LOG = LoggerFactory.getLogger(DistributedOperationCuratorExecutorTest.class);

    private static final long DEFAULT_WAIT_TIME_SECONDS = Long.MAX_VALUE;
//    private static final int ZK_PORT = 53181;  // for CuratorTestServerRule
    private static final int ZK_PORT = 2181;

//    @ClassRule
//    public static final CuratorTestServerRule ZK_TEST_SERVER = new CuratorTestServerRule(ZK_PORT);

    private static final String hosts = "localhost:" + ZK_PORT;
    private long waitTimeSeconds = 80;  //DEFAULT_WAIT_TIME_SECONDS;
    private int baseSleepTimeMills = 1000;
    private int maxRetries = 3;

//    private RetryPolicy retryPolicy;
    private CuratorFramework client;
    private DistributedOperationCuratorExecutor executor;
    private String testLockPath;

    @Before
    public void setUp() {
        testLockPath = "/test-write-lock-" + System.currentTimeMillis();
//        retryPolicy = new ExponentialBackoffRetry(baseSleepTimeMills, maxRetries);
//        client = CuratorFrameworkFactory.newClient(hosts, retryPolicy);

        client = CuratorFrameworkFactory.builder()
                .connectString(hosts)
                .retryPolicy(new ExponentialBackoffRetry(baseSleepTimeMills, maxRetries))
//                .schemaSet(schemaSet)
                .build();

        executor = new DistributedOperationCuratorExecutor(client);
        client.start();
    }

    @After
    public void tearDown() throws Exception {
//        ZooKeeper zk = client.getZookeeperClient().getZooKeeper();
//
//        if (zk.exists(testLockPath, false) == null) {
//            client.close();
//            return;
//        }
//
//        List<String> children = zk.getChildren(testLockPath, false);
//        for (String child : children) {
//            zk.delete(testLockPath + "/" + child, -1);
//        }
//        zk.delete(testLockPath, -1);
//        client.close();

        CloseableUtils.closeQuietly(client);
    }

//    @Test
//    public void testWithLock() throws InterruptedException, KeeperException {
//        LOG.debug("********* START ********************");
//        assertThat(zooKeeper.exists(testLockPath, false), is(nullValue()));
//        executor.withLock("Test Lock", testLockPath, () -> {
//            assertNumberOfChildren(zooKeeper, testLockPath, 1);
//            return null;
//        });
//        assertNumberOfChildren(zooKeeper, testLockPath, 0);
//    }
//
//    @Test
//    public void testWithLockHavingSpecifiedTimeout() throws InterruptedException, KeeperException {
//        LOG.debug("********* START ********************");
//        assertThat(zooKeeper.exists(testLockPath, false), is(nullValue()));
//        final String opResult = "success";
//        DistributedOperationResult<String> result = executor.withLock("Test Lock w/Timeout", testLockPath,
//                () -> opResult, 10, TimeUnit.SECONDS);
//        assertThat(result.timedOut, is(false));
//        assertThat(result.result, is(opResult));
//    }
//
//    @Test
//    public void testWithLockHavingACLAndHavingSpecifiedTimeout() throws InterruptedException, KeeperException {
//        LOG.debug("********* START ********************");
//        assertThat(zooKeeper.exists(testLockPath, false), is(nullValue()));
//        final String opResult = "success";
//        DistributedOperationResult<String> result = executor.withLock("Test Lock w/Timeout", testLockPath, ZooDefs.Ids.OPEN_ACL_UNSAFE,
//                () -> opResult, 10, TimeUnit.SECONDS);
//        assertThat(result.timedOut, is(false));
//        assertThat(result.result, is(opResult));
//    }

    @Test
    public void testWithLockForMultipleLocksInDifferentThreads() throws Exception {
        LOG.debug("********* START ********************");
        ZooKeeper zooKeeper = client.getZookeeperClient().getZooKeeper();
        assertThat(zooKeeper.exists(testLockPath, false), is(nullValue()));

        List<TestDistOp> ops = Arrays.asList(
                new TestDistOp("op-1", "1111_111"),
                new TestDistOp("op-2", "1111_111"),
                new TestDistOp("op-3", "1111_222"),
                new TestDistOp("op-4", "1111_222"),
                new TestDistOp("op-5", "1111_222", true),
                new TestDistOp("op-6", "1111_222", true),
                new TestDistOp("op-7", "1111_111", false),
                new TestDistOp("op-8", "1111_111", false)
        );

        List<Thread> opThreads = new ArrayList<>();
        for (TestDistOp op : ops) {
            opThreads.add(launchDistributedOperation(op));
            Thread.sleep(1);
        }

        long maxWaitTimeMillis = TimeUnit.MINUTES.toMillis(3);
        for (Thread opThread : opThreads) {
            opThread.join(maxWaitTimeMillis);

            LOG.debug("********* testLockPath [{}], TestDistOp.callCount [{}]", testLockPath, TestDistOp.callCount.get());
        }

        assertThat(TestDistOp.callCount.get(), is(ops.size()));
        for (TestDistOp op : ops) {
            assertThat(op.executed.get(), is(true));
        }
    }

    private Thread launchDistributedOperation(final TestDistOp op) {
        Thread opThread = new Thread(() -> {
            try {
                executor.withLock(op.name, op.lockModifier, testLockPath, waitTimeSeconds, op);
            } catch (Exception ex) {
                throw new DistributedOperationException(ex);
            }
        });
        opThread.start();
        return opThread;
    }

    static class TestDistOp implements DistributedOperation<Void> {

        static final AtomicInteger callCount = new AtomicInteger(0);

        final String name;
        final String lockModifier;
        final Boolean isQuick;
        final AtomicBoolean executed;

        TestDistOp(String name, String lockModifier, Boolean isQuick) {
            this.name = name;
            this.lockModifier = lockModifier;
            this.isQuick = isQuick;
            this.executed = new AtomicBoolean(false);
        }

        TestDistOp(String name, String lockModifier) {
            this(name, lockModifier, false);
        }

        @Override
        public Void execute() throws DistributedOperationException {

            if (this.isQuick) {
                doSomeWorkQuick(name);
            } else {
                doSomeWork(name);
            }

            callCount.incrementAndGet();
            LOG.debug("Finished work for [{}], callCount incremented to: [{}] seconds", name, callCount.get());
            executed.set(true);
            LOG.debug("executed set for [{}], executed set to: [{}] seconds", name, executed.get());
            return null;
        }
    }

    private static void doSomeWork(String name) {
        int seconds = new RandomAmountOfWork().timeItWillTake();
        long workTimeMillis = seconds * 1000L;
        LOG.info("{} is doing some work for {} seconds", name, seconds);

        try {
            Thread.sleep(workTimeMillis);
        } catch (InterruptedException ex) {
            LOG.error("Oops. Interrupted.", ex);
            Thread.currentThread().interrupt();
        }
    }

    private static void doSomeWorkQuick(String name) {
        int rendomNumber = new RandomAmountOfWork().timeItWillTake();
//        long workTimeMillis = rendomNumber * 1000L;
        LOG.info("{} is doing some work for {} milliseconds", name, rendomNumber);

        try {
            Thread.sleep(rendomNumber);
        } catch (InterruptedException ex) {
            LOG.error("Oops. Interrupted.", ex);
            Thread.currentThread().interrupt();
        }
    }

//    private void assertNumberOfChildren(ZooKeeper zk, String path, int expectedNumber) {
//        List<String> children;
//        try {
//            children = zk.getChildren(path, false);
//        } catch (Exception ex) {
//            throw new RuntimeException(ex);
//        }
//        assertThat(children.size(), is(expectedNumber));
//    }
}
