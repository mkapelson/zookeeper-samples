package com.nearinfinity.examples.zookeeper.lock;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessLock;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.recipes.locks.LockInternalsDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class DistributedOperationCuratorExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(DistributedOperationCuratorExecutor.class);

    private final CuratorFramework client;

    public DistributedOperationCuratorExecutor(CuratorFramework client) {
        this.client = client;
    }

    public <T> void withLock(String name, String lockModifier, String lockPath, long waitTimeSeconds, DistributedOperation<T> op) throws Exception {
        withLockInternal(name, lockModifier, lockPath, waitTimeSeconds, op);
    }

    private <T> void withLockInternal(String name, String lockModifier, String lockPath, long waitTimeSeconds, DistributedOperation<T> op)
            throws Exception {

        LockInternalsDriver driver = new CcLockInternalsDriver(lockModifier);
        InterProcessLock lock = new InterProcessMutex(client, lockPath, driver);
        if (lock.acquire(waitTimeSeconds, TimeUnit.SECONDS)) {
            LOG.debug("********* lock acquired by name [{}],  lockModifier [{}]", name, lockModifier);
            try {
                op.execute();
            } finally {
                lock.release();
                LOG.debug("********* lock released by name [{}],  lockModifier [{}]", name, lockModifier);
            }
        } else {
            LOG.error("[{}] timed out after [{}] seconds waiting to acquire lock on [{}]",
                    name, waitTimeSeconds, lockPath);
        }

    }

}
